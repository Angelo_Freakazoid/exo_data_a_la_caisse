DROP DATABASE IF EXISTS caisse;
CREATE DATABASE caisse;
USE caisse;

CREATE TABLE address (
  address_id INT PRIMARY KEY AUTO_INCREMENT,
  street varchar(255),
  city varchar(255),
  zipcode INT,
  state varchar(255),
  country varchar(255)
);

CREATE TABLE client (
  client_id INT PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(255),
  name VARCHAR(255),
  phone_num VARCHAR(255),
  default_address_id INT,
  FOREIGN KEY (default_address_id) REFERENCES address(address_id)
);

CREATE TABLE client_address (
  client_id INT,
  address_id INT,
  PRIMARY KEY (client_id, address_id),
  FOREIGN KEY (client_id) REFERENCES client(client_id),
  FOREIGN KEY (address_id) REFERENCES address(address_id)
);

CREATE TABLE product (
  product_id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255),
  description TEXT,
  images VARCHAR(255),
  price FLOAT,
  available BOOLEAN
);

CREATE TABLE orders (
  order_id INT PRIMARY KEY AUTO_INCREMENT,
  client_id INT,
  delivery_address_id INT,
  billing_address_id INT,
  payment_method ENUM('credit card', 'bank check on delivery', 'cash on delivery'),
  status ENUM('cart', 'validated', 'sent', 'delivered'),
  FOREIGN KEY (client_id) REFERENCES client(client_id),
  FOREIGN KEY (delivery_address_id) REFERENCES address(address_id),
  FOREIGN KEY (billing_address_id) REFERENCES address(address_id)
);

CREATE TABLE order_line (
  order_id INT,
  product_id INT,
  quantity INT,
  PRIMARY KEY (order_id, product_id),
  FOREIGN KEY (order_id) REFERENCES orders(order_id),
  FOREIGN KEY (product_id) REFERENCES product(product_id)
);

DROP user IF EXISTS 'david'@'localhost';
create user 'david'@'localhost' identified by 'coucou123';
grant all privileges on caisse.* to 'david'@'localhost';