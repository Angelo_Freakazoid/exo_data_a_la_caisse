from pydantic import BaseModel


class Product(BaseModel):
    product_id: int | None = None
    name: str
    description: str
    images: str
    price: float
    available: bool

class Orders(BaseModel):
    order_id: int | None = None
    client_id: int | None = None
    delivery_address_id: int | None = None
    billing_address_id: int | None = None
    payment_method: str | None = None
    status: str | None = None

class Client(BaseModel):
    client_id: int | None = None
    name: str | None = None
    email: str | None = None
    phone_num: str | None = None
    default_address_id: int | None = None

class Address(BaseModel):
    address_id: int | None = None
    street: str | None = None
    city: str | None = None
    zipcode: int | None = None
    state: str | None = None
    country: str | None = None

class Client_Address(BaseModel):
    client_id: int | None = None
    address_id: int | None = None

class Order_Line(BaseModel):
    order_id: int | None = None
    product_id: int | None = None
    quantity: int | None = None