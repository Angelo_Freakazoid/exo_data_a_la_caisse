# Décrire (à l'écrit) chaque table et ses relations avec les autres tables.

## Table client : 

### elle est composé de 5 données : le mail, le nom, l’id, le numéro de téléphone et l’adresse.

## Table order : 

### elle est composé de 5 données : l’id du client, ligne de commande ? L’adresse de livraison, l’adresse de facturation et la méthode de paiement.

## Table payment method :

### elle est composée de 3 donnée : la carte bancaire, du chèque bancaire à la livraison et du cash à la livraison.

## Table order state :

### elle est composée de 4 données : le panier, la validation, l’envoie et la livraison

## Table order line :

### elle est composée de 3 données : l’id du produit, l’id de la commande et de la quantité.

## Table product :

### elle est composée de 5 données : le nom du produit, la description du produit, les images du produit, le prix du produit et la dipso du produit.


## Table adress :

### Elle est composée de 5 données : le nom de la rue, la ville, le code postal, l’état et le pays (pays fédéral genre USA ?)

## Table client adress :

### Elle est composée de 2 données : l’id du client et l’id de l’adresse.


# Quelle est la particularité de la relation entre Client et Adress ? A quoi peut elle servir ?

### La table Client_Adress indique que le client peut avoir plusieurs adresses et une adresse peut avoir plusieurs clients. 

# Qu'est-ce qu'il y a dans default_adress ?

### default_adress se trouve dans la table Client, elle contient l’adress_id qui permet de relier la table Client et la table Adress. Elle contient l’adresse principale. Vu que plusieurs adresses peuvent être données à un même client. 