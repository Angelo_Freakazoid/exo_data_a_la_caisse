from typing import Union, List, Annotated
import mysql.connector
from fastapi import FastAPI
from fastapi import HTTPException
from main import bdd_login, bdd_mdp
from model_dto import Product, Orders, Client, Address, Client_Address, Order_Line
from pprint import pprint

mydb = mysql.connector.connect(
  host="localhost",
  user=bdd_login,
  password=bdd_mdp, db='caisse'
)

cursor = mydb.cursor(dictionary=True)
app = FastAPI()

# voir les différentes tables sur l'api

@app.get('/product')
def get_product():
    r = cursor.execute("SELECT * from product")
    return cursor.fetchall()


@app.get('/client')
def get_client():
    r = cursor.execute("SELECT * from client")
    return cursor.fetchall()

@app.get('/address')
def get_address():
    r = cursor.execute("SELECT * from address")
    return cursor.fetchall()

@app.get('/orders')
def get_orders():
    r = cursor.execute("SELECT * from orders")
    return cursor.fetchall()

@app.get("/order_line")
def get_order_line():
    cursor.execute("SELECT * FROM order_line")
    return cursor.fetchall()

# trouver un produit sur la talbe "product" via l'id

@app.get('/product/{product_id}')
def get_product(product_id: int):
    query = "SELECT * FROM product WHERE product_id = %s"
    cursor.execute(query, (product_id,))
    result = cursor.fetchone()
    if result:
        return result 
    else:
        return {"message": "Product not found"}

# ajouter un produit dans la table "product"

@app.post("/product")
def create_product(product: Product):
    mycursor = mydb.cursor()
    sql = "INSERT INTO product (name, description, images, price, available) VALUES (%s, %s, %s, %s, %s)"
    val = (product.name, product.description, product.images, product.price, product.available)
    mycursor.execute(sql, val)
    mydb.commit()
    product_id = mycursor.lastrowid
    created_product = {
        "product_id": product_id,
        "name": product.name,
        "description": product.description,
        "images": product.images,
        "price": product.price,
        "available": product.available
    }
    return created_product

# ajouter un produit dans la table "product" avec son id

@app.post("/product/{product_id}")
def add_product_with_id(product_id: int, product: Product):
    cursor.execute("SELECT * FROM product WHERE product_id = %s", (product_id,))
    if cursor.fetchone():
        return {"message": "Un produit avec la même ID existe déjà !"}

    insert_query = """
        INSERT INTO product (product_id, name, description, images, price, available)
        VALUES (%s, %s, %s, %s, %s, %s)
    """
    cursor.execute(insert_query, (product_id, product.name, product.description, product.images, product.price, product.available))
    mydb.commit()

    return {
        "product_id": product_id,
        "name": product.name,
        "description": product.description,
        "images": product.images,
        "price": product.price,
        "available": product.available
    }

# update un produit

@app.put("/product/{product_id}")
def update_product(product_id: int, product: Product):
    cursor.execute("SELECT * FROM product WHERE product_id = %s", (product_id,))
    if cursor.fetchone() is None:
        return {"message": "Product not found"}
    
    update_query = """
        UPDATE product
        SET name = %s, description = %s, images = %s, price = %s, available = %s
        WHERE product_id = %s
    """
    cursor.execute(update_query, (product.name, product.description, product.images, product.price, product.available, product_id))
    mydb.commit()

    return {
        "product_id": product_id,
        "name": product.name,
        "description": product.description,
        "images": product.images,
        "price": product.price,
        "available": product.available
    }

# supprimer un produit via son ID dans la table "product" 

@app.delete("/product/{product_id}")
def delete_product(product_id: int):
    cursor.execute("SELECT * FROM product WHERE product_id = %s", (product_id,))
    if cursor.fetchone() is None:
        return {"message": "Product not found"}

    delete_query = "DELETE FROM product WHERE product_id = %s"
    cursor.execute(delete_query, (product_id,))
    mydb.commit()

    return {"message": f"Le produit avec l'ID {product_id} a bien été suprimé !"}   

# trouver un client sur la table "client"

@app.get("/client")
async def create_client() -> list:
    mycursor = mydb.cursor(dictionary=True)
    sql = "select * from client;"
    mycursor.execute(sql)
    liste_de_client = mycursor.fetchall()
    return liste_de_client

# trouver un client sur la table "client" avec son ID

@app.get("/client/{client_id}")
def get_client(client_id: int):
    mycursor = mydb.cursor(dictionary=True)
    query = "SELECT * FROM client WHERE client_id = %s"
    mycursor.execute(query, (client_id,))
    client = mycursor.fetchone()

    if client:
        return client
    else:
        return {"message": "Client not found"}

# ajouter un client dans la table "client"

@app.post("/client")
def create_client(client: Client):
    insert_query = """
        INSERT INTO client (email, name, phone_num, default_address_id)
        VALUES (%s, %s, %s, %s)
    """
    cursor.execute(insert_query, (client.email, client.name, client.phone_num, client.default_address_id))
    mydb.commit()
    client_id = cursor.lastrowid

    return {
        "client_id": client_id,
        "email": client.email,
        "name": client.name,
        "phone_num": client.phone_num,
        "default_address_id": client.default_address_id
    }

# ajouter un client dans la table "client" avec son ID

@app.post("/client/{client_id}")
def create_client_with_specific_id(client_id: int, client: Client):
    if client_id != client.client_id:
        raise HTTPException(status_code=400, detail="L'ID client de l'url de correspond pas à l'id du body !")

    cursor.execute("SELECT * FROM client WHERE client_id = %s", (client_id,))
    if cursor.fetchone():
        return {"message": "Un client avec la même ID existe déjà !"}

    insert_query = """
        INSERT INTO client (client_id, email, name, phone_num, default_address_id)
        VALUES (%s, %s, %s, %s, %s)
    """
    cursor.execute(insert_query, (client_id, client.email, client.name, client.phone_num, client.default_address_id))
    mydb.commit()

    return {
        "client_id": client_id,
        "email": client.email,
        "name": client.name,
        "phone_num": client.phone_num,
        "default_address_id": client.default_address_id
    }

# supprimer un produit via son ID dans la table "product" 

@app.delete("/client/{client_id}")
def delete_client(client_id: int):
    cursor.execute("SELECT * FROM client WHERE client_id = %s", (client_id,))
    if cursor.fetchone() is None:
        return {"message": "Client not found"}

    delete_query = "DELETE FROM client WHERE client_id = %s"
    cursor.execute(delete_query, (client_id,))
    mydb.commit()

    return {"message": f"Le client avec l'ID {client_id} a bien été supprimé !"}

# ajouter une commande dans la table orders

@app.put("/orders")
def create_order(order: Orders):
    cursor.execute("SELECT * FROM orders WHERE order_id = %s", (order.order_id,))
    if cursor.fetchone():
        return {"message": "Une commande avec le même ID existe déjà !"}

    insert_query = """
        INSERT INTO orders (order_id, client_id, delivery_address_id, billing_address_id, payment_method, status)
        VALUES (%s, %s, %s, %s, %s, %s)
    """
    cursor.execute(insert_query, (order.order_id, order.client_id, order.delivery_address_id, order.billing_address_id, order.payment_method, order.status))
    mydb.commit()

    return {
        "order_id": order.order_id,
        "client_id": order.client_id,
        "delivery_address_id": order.delivery_address_id,
        "billing_address_id": order.billing_address_id,
        "payment_method": order.payment_method,
        "status": order.status
    }

# trouver une commande dans la table "orders" par son id

@app.get("/orders/{order_id}")
def get_order_details(order_id: int):
    cursor.execute("SELECT * FROM orders WHERE order_id = %s", (order_id,))
    order_details = cursor.fetchall()

    if order_details:
        return order_details
    else:
        return {"message": "Order not found"}

# ajouter une adresse à un client via la table client_address

@app.post("/client_address")
async def associate_address_to_client(association: Client_Address):
    cursor.execute("SELECT * FROM client_address WHERE client_id = %s AND address_id = %s", (association.client_id, association.address_id))
    if cursor.fetchone():
        return {"message": "Cette association client-adresse existe déjà"}

    cursor.execute("SELECT * FROM client WHERE client_id = %s", (association.client_id,))
    if not cursor.fetchone():
        raise HTTPException(status_code=404, detail="Client non trouvé")

    cursor.execute("SELECT * FROM address WHERE address_id = %s", (association.address_id,))
    if not cursor.fetchone():
        raise HTTPException(status_code=404, detail="Adresse non trouvée")

    cursor.execute("INSERT INTO client_address (client_id, address_id) VALUES (%s, %s)", (association.client_id, association.address_id))
    mydb.commit()

    return {"message": "Adresse associée au client avec succès"}


# récupérer l'id du client et l'adresse du client dans la table client_address    

@app.get("/client_address", response_model=List[Client_Address])
async def get_client_address():
    cursor.execute("SELECT client_id, address_id FROM client_address")
    client_addresses = cursor.fetchall()

    if not client_addresses:
        raise HTTPException(status_code=404, detail="Aucune association client-adresse trouvée")

    return client_addresses

# récupérer les adresses d'un client via la table client_address

@app.get("/client/{client_id}/address", response_model=List[Address])
def get_client_addresses(client_id: int):
    query = """
    SELECT a.* FROM address a
    INNER JOIN client_address ca ON a.address_id = ca.address_id
    WHERE ca.client_id = %s
    """
    cursor.execute(query, (client_id,))
    result = cursor.fetchall()

    if not result:
        raise HTTPException(status_code=404, detail="Client not found or no addresses associated")

    return [Address(**address) for address in result]

# Retourner la liste des ids des commandes du client.

@app.get("/order/byClient/{client_id}", response_model=Order_Line)
def get_orders_by_client(client_id: int):
    try:
        query = "SELECT order_id FROM orders WHERE client_id = %s"
        cursor.execute(query, (client_id,))
        result = cursor.fetchall()
        if not result:
            raise HTTPException(status_code=404, detail="Aucune commande trouvée pour ce client")

        order_ids = [row['order_id'] for row in result]
        return Order_Line(order_ids=order_ids)

    except mysql.connector.Error as err:
        
        raise HTTPException(status_code=500, detail="Erreur de base de données")

# Ajoute ou met à jour une ligne de commande.

@app.put("/order/{order_id}/{product_id}/{quantity}", response_model=Order_Line)
def add_or_modify_order_line(order_id: int, product_id: int, quantity: int):
    cursor.execute("SELECT * FROM order_line WHERE order_id = %s AND product_id = %s", (order_id, product_id))
    order_line = cursor.fetchone()
    if order_line:
        cursor.execute("UPDATE order_line SET quantity = %s WHERE order_id = %s AND product_id = %s", (quantity, order_id, product_id))
    else:
        cursor.execute("INSERT INTO order_line (order_id, product_id, quantity) VALUES (%s, %s, %s)", (order_id, product_id, quantity))
    mydb.commit()
    return {
        "order_id": order_id,
        "product_id": product_id,
        "quantity": quantity
    }

# Assigner une commande à un client 

@app.put("/order/{order_id}/{client_id}")
def assign_order_to_client(order_id: int, client_id: int):
    cursor.execute("SELECT * FROM orders WHERE order_id = %s", (order_id,))
    if cursor.fetchone() is None:
        raise HTTPException(status_code=404, detail="La commande n'existe pas.")

    cursor.execute("SELECT * FROM client WHERE client_id = %s", (client_id,))
    if cursor.fetchone() is None:
        raise HTTPException(status_code=404, detail="Le client n'existe pas.")

    cursor.execute("SELECT client_id FROM orders WHERE order_id = %s", (order_id,))
    existing_assignment = cursor.fetchone()
    if existing_assignment and existing_assignment['client_id'] is not None:
        raise HTTPException(status_code=400, detail="Cette commade est déjà assignée à un client.")

    update_query = "UPDATE orders SET client_id = %s WHERE order_id = %s"
    cursor.execute(update_query, (client_id, order_id))
    mydb.commit()

    return {"message": f"La commande {order_id} a bien été assignée au client {client_id} !"}

# Retourne 200 si ok. Si un client est associé à la commande et que l'adresse envoyée n'appartient pas au client, doit renvoyer 400

@app.put("/order{order_id}?delivery={adress_id}")
def update_order_delivery_address(order_id: int, delivery_address_id: int):
    cursor.execute("SELECT * FROM orders WHERE order_id = %s", (order_id,))
    order = cursor.fetchone()
    if not order:
        raise HTTPException(status_code=404, detail="Commande non trouvée")

    cursor.execute("SELECT * FROM address WHERE address_id = %s", (delivery_address_id,))
    if not cursor.fetchone():
        raise HTTPException(status_code=404, detail="Adresse non trouvée")

    if order['client_id']:
        cursor.execute("SELECT * FROM client_address WHERE client_id = %s AND address_id = %s", (order['client_id'], delivery_address_id))
        if not cursor.fetchone():
            raise HTTPException(status_code=400, detail="L'adresse n'appartient pas au client associé à la commande")

    update_query = "UPDATE orders SET delivery_address_id = %s WHERE order_id = %s"
    cursor.execute(update_query, (delivery_address_id, order_id))
    mydb.commit()

    return {"message": "L'adresse de livraison a bien été mise à jour !"}
